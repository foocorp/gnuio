<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">GNU social:</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="/social">Home</a></li>
        <li><a href="/social/about/">About</a></li>
        <li><a href="/social/contact/">Contact</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Resources <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="/social/resources/">Mailing List</a></li>
            <li><a href="/social/resources/code/">Download source code</a></li>
            <li><a href="/social/resources/who/">Who's who?</a></li>
            <li><a href="/social/resources/faq/">FAQ</a></li>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-right">
        <a type="submit" class="btn btn-primary" href="/social/resources/code">Download GNU social</a>
        <a type="submit" class="btn btn-warning" href="/social/resources/">Join the list</a>
        <a type="submit" class="btn btn-success" href="/social/try/">Sign up</a>
      </form>
    </div><!--/.navbar-collapse -->
  </div>
</div>
